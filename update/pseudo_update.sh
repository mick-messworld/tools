#!/usr/bin/env bash

# Copyright (C) 2010-2012  TMW2 Online
# Author: Andrei Karas (4144)

dir=`pwd`

previous=`cat commit.txt`
rm upload/Extra.zip

cd ../../client-data
head=`git log --pretty=oneline -n 1 | awk '{print $1}'`
u1=`echo ${previous} | cut -c 1-7`
u2=`echo ${head} | cut -c 1-7`
git diff --name-status ${previous} HEAD | awk '/^(A|M)\t/ {print $2}; /^(R...)\t/ {print $3}' | \
grep -e "[.]\(xml\|png\|tmx\|ogg\|txt\|po\|tsx\|md\)" | sort | uniq | \
xargs zip -X -9 -r ../tools/update/upload/Extra.zip

cd $dir/upload

sum=`adler32 Extra.zip | awk '{print $2}'`
echo "Update ID: ${u1}..${u2}"
echo "Checksum: ${sum}"

echo "Extra.zip ${sum}" >>resources2.txt
cp ../files/xml_header.txt resources.xml
echo "    <update type=\"data\" file=\"Extra.zip\" hash=\"${sum}\" />" >> resources.xml
#cat ../files/xml_footer.txt >>resources.xml
#cat ../files/xml_mods.txt >>resources.xml
echo '</updates>' >>resources.xml

