#!/usr/bin/python2.7

class Item:
  def __init__(self, xid):
    self.id=xid
    self.lvl=0

a=open("../../client-data/items.xml", "r")

swords=[]
bows=[]
shields=[]

gid="0"
rid=0
ctx=Item(0)
mem=[]

for l in a:
    if "<item id=" in l:
        if ctx.id > 0:
            mem.append(ctx)

        gid=l.replace('\t', '').replace(' ','').replace('<itemid=', '').replace('"', '').replace("'", "")
        rid=0
        if "-" in gid:
            gid="0"
            continue
        try:
            rid=int(gid)
        except:
            print "[CRITICAL] Invalid item ID format: " + l
            exit(1)

        ctx=Item(rid)

    if "\tlevel=" in l or " level=" in l:
        gid=l.replace('\t', '').replace(' ','').replace('level=', '').replace('"', '').replace("'", "")
        try:
            rid=int(gid)
        except:
            print "[CRITICAL] Invalid item level format: " + l
            rid=0
        ctx.lvl=0+rid

mem=sorted(mem, key=lambda xcv: xcv.lvl, reverse=True)

for r in mem:
    rid=r.id
    if rid >= 2700 and rid <= 2899:
        shields.append(rid)
    elif rid >= 3500 and rid <= 3999:
        swords.append(rid)
    elif rid >= 6000 and rid <= 6499:
        bows.append(rid)

a.close()

#shields=sorted(shields, reverse=True)
#bows=sorted(bows, reverse=True)
#swords=sorted(swords, reverse=True)

b=open("weapons.tmp", "w")

b.write('<?xml version="1.0" encoding="utf-8"?>\n\
<!-- Author: 4144, Jesusalva\n\
Copyright (C) 2015 Evol Online\n\
Copyright (C) 2019-2020 The Mana World\n -->\n\
\n\
<weapons>\n')

b.write('    <swords>\n')

for i in swords:
    b.write('        <item id="%d"/>\n' % i)

b.write('    </swords>\n    <bows>\n')

for i in bows:
    b.write('        <item id="%d"/>\n' % i)

b.write('    </bows>\n    <shields>\n')

for i in shields:
    b.write('        <item id="%d"/>\n' % i)

b.write('    </shields>\n</weapons>')

b.close()
