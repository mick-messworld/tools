#!/usr/bin/python2.7

# Setup
x=y=0
i=j=0

# Functions
def headers(val):
    return '\n\t<dialog name="daily_%d" hideText="true">\n\t\t<menu>\n\t\t\t<text x="45" y="0" width="310" height="30" text="##BDaily Login Rewards##b" />\n            <button x="157" y="280" name="Claim" value="Ok" />\n\n' % val

def tail():
    return '\n\t\t</menu>\n\t</dialog>\n'

def override_check(vl, over, text1, text2):
    global j
    if not over:
        j=0
        return '\t\t\t<image %s image="graphics/images/%s.png" />\n' % (text1, text2)
    else:
        j+=1
        if (j == vl):
            return '\t\t\t<image %s image="graphics/images/done.png" />\n' % (text1)
        elif (j < vl):
            return '\t\t\t<image %s image="graphics/images/ok.png" />\n' % (text1)
        else:
            return ""

def spammer(val, override=False):
    bf=""

    bf+=override_check(val, override, 'x="35" y="35"', 'jexp')
    bf+=override_check(val, override, 'x="70" y="35"', 'bexp')
    bf+=override_check(val, override, 'x="105" y="35"', 'sc')
    bf+=override_check(val, override, 'x="140" y="35"', 'jexp')
    bf+=override_check(val, override, 'x="175" y="35"', 'bexp')

    bf+=override_check(val, override, 'x="35" y="70"', 'gp')
    bf+=override_check(val, override, 'x="70" y="70"', 'sc')
    bf+=override_check(val, override, 'x="105" y="70"', 'bexp')
    bf+=override_check(val, override, 'x="140" y="70"', 'gp')
    bf+=override_check(val, override, 'x="175" y="70"', 'jexp')

    bf+=override_check(val, override, 'x="35" y="105"', 'bexp')
    bf+=override_check(val, override, 'x="70" y="105"', 'gp')
    bf+=override_check(val, override, 'x="105" y="105"', 'jexp')
    bf+=override_check(val, override, 'x="140" y="105"', 'gift')
    bf+=override_check(val, override, 'x="175" y="105"', 'gp')

    bf+=override_check(val, override, 'x="35" y="140"', 'jexp')
    bf+=override_check(val, override, 'x="70" y="140"', 'bexp')
    bf+=override_check(val, override, 'x="105" y="140"', 'gp')
    bf+=override_check(val, override, 'x="140" y="140"', 'jexp')
    bf+=override_check(val, override, 'x="175" y="140"', 'bexp')

    bf+=override_check(val, override, 'x="35" y="175"', 'gift')
    bf+=override_check(val, override, 'x="70" y="175"', 'jexp')
    bf+=override_check(val, override, 'x="105" y="175"', 'bexp')
    bf+=override_check(val, override, 'x="140" y="175"', 'gp')
    bf+=override_check(val, override, 'x="175" y="175"', 'jexp')

    bf+=override_check(val, override, 'x="35" y="210"', 'bexp')
    bf+=override_check(val, override, 'x="70" y="210"', 'gp')
    bf+=override_check(val, override, 'x="105" y="210"', 'last')
    bf+=override_check(val, override, 'x="140" y="210"', 'sc')
    bf+=override_check(val, override, 'x="175" y="210"', 'sc')

    bf+=override_check(val, override, 'x="35" y="245"', 'sc')

    return bf

# Begin
f=open("daily.tmp", "w")

f.write('<?xml version="1.0" encoding="utf-8"?>\n<!-- This file is generated automatically, editing it will have no effect.\n     (C) Jesusalva, 2019 -->\n<dialogs>')

while (i < 31):
    i+=1
    f.write(headers(i))
    f.write(spammer(i, False))
    f.write('\n\t\t\t<image x="245"  y="52" image="graphics/images/final.png" />\n\n\t\t\t<!-- Complete -->\n')
    f.write(spammer(i, True))
    f.write(tail())

f.write('\n</dialogs>')
f.close()

