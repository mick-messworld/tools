#!/usr/bin/env bash

sleep 2

export MANAPLUS="../../manaplus/src/manaplus"
if [ -f "$MANAPLUS" ]; then
    echo "Starting local manaplus"
else
    echo "Starting system manaplus"
    export MANAPLUS="manaplus"
fi

nice -n 18 ${MANAPLUS} -u -d ../../client-data evol.manaplus
